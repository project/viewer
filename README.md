CONTENTS OF THIS FILE
---------------------

 * Introduction to Viewer
 * Requirement
 * Installation
 * Maintainers


# Introduction to Viewer

* The Viewer module is a simple and yet powerful module to import and process
 structured or unstructured file types such as CSV, XLSX/XLS, PDF and present
 them in Drupal via Viewer reference field or a Block.
* This module comes with multiple plugin support for various data. Some of the
 plugins include integration with Chart.js, ApexCharts, Datatables, and etc.
* The user may easily organize your data and display each file in a tab
 (horizontal or vertical), accordions and other options could be also added.
* This module can import files from different locations it could be FTP, SFTP,
 absolute path, remote URL or manual upload.
* Automated imports on cron are also supported.
* The module could be useful to show financial information, analytical data or
 any other files on your site.

* For a full description of the module, visit the
[project page](https://www.drupal.org/project/viewer).

* Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/viewer).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Basic use

1. Create a Source at /admin/structure/viewer-source. This can a PDF, CSV or 
   XLSX file referenced from a URL or path, or loaded via S/FTP or file upload.
2. Create one or more Viewers from this source at /admin/structure/viewers. 
   This Viewer can render the source as a table or chart, with descriptive 
   text fields such as a title, subtitle or summary.
3. The Viewer can be embedded into the site:
    1. As a Viewer block (via /admin/structure/block).
    2. Referenced through a custom field in a content type 
      (add a General > Viewer field to the Content type).
    3. Inserted by CKEditor in Long Text fields.
       Edit or create a new text format at /admin/config/content/formats:
        * Select CKEditor 5 as the Text Editor
        * Add the Viewer button to the CKEditor toolbar
        * Enable the Viewer filter 

## MAINTAINERS

Current maintainers:

* Minnur Yunusov (https://www.drupal.org/u/minnur)
